﻿# Drawe case

Drawer case for electronic monitor

Modeling in FreeCAD

## Measures of the dresser
Length 16cm, Width 9 Cm, And Height 6 Cm.




## Images
![drawer_case photo_1](/Drawer_case/drawer_case_1.jpg)
![drawer_case photo_2](/Drawer_case/drawer_case_2.jpg)
![drawer_case render](/Drawer_case/drawer_case.png)

## License
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions
See commit details to find the authors of each Part.
- @fandres7_7
