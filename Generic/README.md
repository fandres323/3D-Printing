﻿# Generic parts

Modeling in FreeCAD


## Shower Curtain Hook 
![freeCAD rendering](/Generic/Gancho_cortina_baño.png)


## License
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions

See commit details to find the authors of each Part.
- @fandres7_7

## See more in the repository
[Gitlab](https://gitlab.com/fandres323/3D-Printing)
