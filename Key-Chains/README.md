﻿# Model 3D on FreeCad
============

# Key-Chains
![3D_Key-Chains](/Key-Chains/Key-Chain-Hole.png)
![3D_Key-Chain-Rec](/Key-Chains/Key-Chain-Rec.png)

# License 
======

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

# Attributions
============
See commit details to find the authors of each Part.
- @fandres7_7
