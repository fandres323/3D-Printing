﻿# ESP8266 Programing

- [PCB Kicad](/ESP12E_Programing/PCB/)
- [Socket ESP8266 (ESP12E)](/ESP12E_Programing/Socket/)

## Images
![Print](/ESP12E_Programing/Socket/Print.jpg)
![pcb](/ESP12E_Programing/PCB/PCB/Socket_ESP12E-B.Cu.pdf)

## 3D Print
- [Socket (STL)](/ESP12E_Programing/Socket/Socket_ESP12E.stl)

## PCB
- [PCB (PDF)](/ESP12E_Programing/PCB/PCB/Socket_ESP12E-B.Cu.pdf)
- [PCB (Gerber)](/ESP12E_Programing/PCB/PCB/)

## License
Copyright 2018, S. Fabian (fAnDREs)
- Socket Model: [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-nc-sa/4.0/)
- PCB [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

## Attributions
### Socket
Based on the work of [WDWHITE ](https://www.thingiverse.com/thing:1333056)
Step File by by_Andrey_Chirva

### See commit details to find the authors of each Part.
- @fandres7_7
