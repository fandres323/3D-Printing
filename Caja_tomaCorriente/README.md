﻿# Caja Toma-corriente

- Modeling in FreeCAD


## Images
![Caja_tomaCorriente 1](/Caja_tomaCorriente/Caja_toma_corriente.jpg)
![Caja_tomaCorriente 1](/Caja_tomaCorriente/Caja_toma_corriente.png)

## License
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)


## Attributions
See commit details to find the authors of each Part.
- @fandres7_7
