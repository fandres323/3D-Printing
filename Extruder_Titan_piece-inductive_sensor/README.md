﻿# Model 3D on FreeCad

# Config Marlin

//===========================================================================
//=================================== Mesh ==================================
//===========================================================================
//Do not forget to enable safe homing.
#define Z_SAFE_HOMING

//===========================================================================
//============================= Z Probe Options =============================
//===========================================================================
//...
#define X_PROBE_OFFSET_FROM_EXTRUDER 5 //10  // X offset: -left  +right  [of the nozzle]
#define Y_PROBE_OFFSET_FROM_EXTRUDER 37 //10  // Y offset: -front +behind [the nozzle]
#define Z_PROBE_OFFSET_FROM_EXTRUDER 0 //-0.4  // Z offset: -below +above  [the nozzle]

# Images
![3D_Extruder_Titan_piece-inductive_sensor](/Extruder_Titan_piece-inductive_sensor/Extruder_Titan_piece-inductive_sensor.png)

# License 

[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)

# Attributions
See commit details to find the authors of each Part.
- @fandres7_7
